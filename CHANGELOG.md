# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [1.0.0]
### Fixed
-  Fix constraint for MovieName UILabel in Main.storyboard
### Refactor
- Refactor Code for CellForRowAtIndexPath
- Removed unused Code from Appdelegate to keep cleaner
