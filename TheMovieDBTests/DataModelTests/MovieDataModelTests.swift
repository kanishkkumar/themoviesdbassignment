//
//  MovieDataModelTests.swift
//  TheMovieDBTests
//
//  Created by Kanishk KUMAR on 14/5/2018.
//  Copyright © 2018 mycompany. All rights reserved.
//

import Foundation
import Quick
import Nimble
@testable import TheMovieDB

class MovieDataModelTests: QuickSpec {

    override func spec() {
        let movie = MovieDataModel(title: "avenger", releaseDate: "9-05-2018", rating: 9.0, overview: "overiew of the movie", posterPath:"2DtPSyODKWXluIRV7PVru0SSzja.jpg​")
        let movieAPIDataResponse = MovieApiDataResponse(page: 1, numberOfResults: 1, numberOfPages: 1, movies: [movie])

        describe("Verify MovieDataModel model's attributes") {
            context("when initialized with poster path", closure: {

                it("should have correct movieName", closure: {
                    expect(movie.title) == "avenger"
                })

                it("should have correct Release Date", closure: {
                    expect(movie.releaseDate) == "9-05-2018"
                })

                it("should have correct Rating Date", closure: {
                    expect(movie.rating) == 9.0
                })

                it("should have Overview", closure: {
                    expect(movie.overview) == "overiew of the movie"
                })

                it("should have correct Poster Path", closure: {
                    expect(movie.posterPath) == "2DtPSyODKWXluIRV7PVru0SSzja.jpg​"
                })

                it("should have correct Poster URL", closure: {
                    expect(movie.posterUrl) == "https://image.tmdb.org/t/p/w922DtPSyODKWXluIRV7PVru0SSzja.jpg​"
                })
            })
        }

        describe("Verify MovieAPIDataResponse model's attributes") {
            context("when initialized with Movie Data", closure: {

                it("should have correct PageNumber", closure: {
                    expect(movieAPIDataResponse.page) == 1
                })

                it("should have correct NoOfResults", closure: {
                    expect(movieAPIDataResponse.numberOfResults) == 1
                })

                it("should have correct NumberOfPages", closure: {
                    expect(movieAPIDataResponse.numberOfPages) == 1
                })

                it("should have atleast 1 movie", closure: {
                    expect(movieAPIDataResponse.movies.count) > 0
                })

                it("should have atleast 1 movie with Title", closure: {
                    let movie = movieAPIDataResponse.movies[0]
                    expect(movie.title) == "avenger"
                })

                it("should have atleast 1 movie with Correct Rating", closure: {
                    let movie = movieAPIDataResponse.movies[0]
                    expect(movie.rating) == 9.0
                })

                it("should have atleast 1 movie with Correct Release Date", closure: {
                    let movie = movieAPIDataResponse.movies[0]
                    expect(movie.releaseDate) == "9-05-2018"
                })

                it("should have atleast 1 movie with correct Overview", closure: {
                    expect(movie.overview) == "overiew of the movie"
                })

                it("should have atleast 1 movie with correct Poster Path", closure: {
                    expect(movie.posterPath) == "2DtPSyODKWXluIRV7PVru0SSzja.jpg​"
                })

                it("should have atleast 1 movie with correct Poster URL", closure: {
                    expect(movie.posterUrl) == "https://image.tmdb.org/t/p/w922DtPSyODKWXluIRV7PVru0SSzja.jpg​"
                })
            })
        }
    }
}
