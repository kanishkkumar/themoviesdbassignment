//
//  TheMoviesCoreDataTests.swift
//  TheMovieDBTests
//
//  Created by Kanishk KUMAR on 14/5/2018.
//  Copyright © 2018 mycompany. All rights reserved.
//

import Foundation
import Quick
import Nimble
import CoreData
@testable import TheMovieDB

class TheMoviesCoreDataTests: QuickSpec {

    let apiManager = APIManager()
    override func spec() {
        describe("Verify the Core Data params") {

            context("when initialized", closure: {

                it("should return the movieName/searched term from DB", closure: {
                    let results = CoreDataManager.fetchResultsFromDB()
                    //Assert: return movieName item
                    XCTAssertNotNil(results?.count)
                })

                it("should save the movieName/searched term from DB without any error", closure: {
                    let movieName = "avenger"
                    //When add movieName
                    _ = CoreDataManager.saveValueIntoDB(_value: movieName)
                })

                it("should return the searched term from DB", closure: {
                    let results = CoreDataManager.getSearchTexts()
                    //Assert: return movieName item
                    XCTAssertNotNil(results?.count)
                })

                it("should save the movieName/searched term and return the searched term from DB", closure: {
                    let movieName = "avenger"
                    //When add movieName
                    CoreDataManager.saveValueIntoDB(_value: movieName)
                    let results = CoreDataManager.fetchResultsFromDB()
                    XCTAssertNotNil(results?.count)
                })

                it("should return the movieName/searched term LessThanOrEqualTo 10", closure: {
                    let results = CoreDataManager.fetchResultsFromDB()
                    //Assert: return movieName item should be LessThanOrEqual 10
                    XCTAssertLessThanOrEqual(results!.count,10)
                })
            })
        }
    }
}

