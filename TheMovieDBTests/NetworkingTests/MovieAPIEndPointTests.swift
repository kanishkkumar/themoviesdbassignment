//
//  MovieAPIEndPointTests.swift
//  TheMovieDBTests
//
//  Created by Kanishk KUMAR on 14/5/2018.
//  Copyright © 2018 mycompany. All rights reserved.
//

import Foundation
import Quick
import Nimble
@testable import TheMovieDB

class MovieAPIEndPointTests: QuickSpec {

    var apiManager = APIManager.self
    override func spec() {
        describe("Verify APIManager") {
            context("when initialized", closure: {
                it("should have correct env as production", closure: {
                    expect(self.apiManager.environment) == APIEnvironment.production
                })

                it("should have correct movie API Key", closure: {
                    expect(self.apiManager.MovieAPIKey) == "2696829a81b1b5827d515ff121700838"
                })
            })

            context("when service request returns movies", closure: {
                it("should be successfull", closure: {
                    let apiManager = MockSearchMoviesWithSuccessResults()
                    apiManager.searchMovies(searchTerm: "avenger", page: 1, completion: { (apiResponse, error) in
                        let apiResponse = apiResponse as! MovieApiDataResponse;
                        expect(apiResponse.numberOfPages) == 1000
                        expect(apiResponse.page) == 5
                        expect(apiResponse.numberOfResults) == 60
                        expect(apiResponse.movies.count) == 1
                    })
                })
            })

            context("when service request returns 0 movies", closure: {
                it("should be successfull", closure: {
                    let apiManager = MockSearchMoviesWithEmptyResults()
                    apiManager.searchMovies(searchTerm: "avenger", page: 1, completion: { (apiResponse, error) in
                        let apiResponse = apiResponse as! MovieApiDataResponse;
                        expect(apiResponse.numberOfPages) == 0
                        expect(apiResponse.page) == 0
                        expect(apiResponse.numberOfResults) == 0
                        expect(apiResponse.movies.count) == 0
                    })
                })
            })

            context("when service request fail", closure: {
                it("should be successfull", closure: {
                    let apiManager = MockSearchMoviesFailure()
                    apiManager.searchMovies(searchTerm: "", page: 1, completion: { (apiResponse, error) in
                        expect(error) == "invalid request"
                    })
                })
            })
        }
    }
}
