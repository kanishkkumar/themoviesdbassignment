//
//  MockAPIManager.swift
//  TheMovieDBTests
//
//  Created by Kanishk KUMAR on 14/5/2018.
//  Copyright © 2018 mycompany. All rights reserved.
//

import Foundation
@testable import TheMovieDB

class MockSearchMoviesWithSuccessResults {

    func searchMovies(searchTerm: String, page: Int, completion: @escaping (AnyObject?,_ error: String?)->()) {
        let movie = MovieDataModel(title: "avenger", releaseDate: "releasedate", rating: 9.0, overview: "overview", posterPath: "posterPath")
        let results = MovieApiDataResponse(page: 5, numberOfResults: 60, numberOfPages: 1000, movies: [movie])
        completion(results as AnyObject, nil)
    }
}

class MockSearchMoviesWithEmptyResults {

    func searchMovies(searchTerm: String, page: Int, completion: @escaping (AnyObject?,_ error: String?)->()) {
        let results = MovieApiDataResponse(page: 0, numberOfResults: 0, numberOfPages: 0, movies: [])
        completion(results as AnyObject, nil)
    }
}

class MockSearchMoviesFailure {

    func searchMovies(searchTerm: String, page: Int, completion: @escaping (AnyObject?,_ error: String?)->()) {
        completion(nil, "invalid request")
    }
}
