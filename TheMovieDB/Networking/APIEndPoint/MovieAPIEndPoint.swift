//
//  MovieAPIEndPoint.swift
//  TheMovieDB
//
//  Created by Kanishk KUMAR on 9/5/2018.
//  Copyright © 2018 mycompany. All rights reserved.
//

import Foundation

enum APIEnvironment {
    case production
    case testbed
    case staging
    case development
}

/// API Name, Here its a API to search the movies. Add more cases as per requirnment in future, for eg: recommended movies, new movies etc.
///
/// - searchMovies: Search movie with the search term and page number base on API.
public enum MovieApi {
    case searchMovies(searchTerm: String, page: Int)
}

// MARK: - MovieApi Extension
extension MovieApi: APIEndPointType {

    /// API Endpoint URL, which specifices the Environment you are using. Change the URL based on new Env in furture.
    var environmentBaseURL : String {
        switch APIManager.environment {
        case .production: return "http://api.themoviedb.org/3/"
        case .testbed: return "http://api.themoviedb.org/3/"
        case .staging: return "http://api.themoviedb.org/3/"
        case .development: return "http://api.themoviedb.org/3/"
        }
    }

    var baseURL: URL {
        guard let url = URL(string: environmentBaseURL) else { fatalError("Unable to configure the URL.")}
        return url
    }

    /// Defines the Path for the URL of API
    var path: String {
        switch self {
        case .searchMovies:
            return "search/movie"
        }
    }

    /// Speficies the HTTP Method, GET in this case.
    var httpMethod: HTTPMethod {
        return .get
    }
    
    var httptask: HTTPTask {
        switch self {
        case .searchMovies(let searchText, let page):
            return .requestParameters(bodyParameters: nil,
                                      bodyEncoding: .urlEncoder,
                                      urlParameters: ["page":page,
                                                      "query": searchText,
                                                      "api_key":APIManager.MovieAPIKey])
        }
    }

    var httpheaders: HTTPHeaders? {
        return nil
    }
}
