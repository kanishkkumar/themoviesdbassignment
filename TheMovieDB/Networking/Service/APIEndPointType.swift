//
//  APIEndPointType.swift
//  TheMovieDB
//
//  Created by Kanishk KUMAR on 8/5/2018.
//  Copyright © 2018 mycompany. All rights reserved.
//

import Foundation

/// consists of multiple HTTP Protocls
protocol APIEndPointType {

    // MARK: - Properties
    var baseURL: URL { get }
    var path: String { get }
    var httpMethod: HTTPMethod { get }
    var httptask: HTTPTask { get }
    var httpheaders: HTTPHeaders? { get }
}
