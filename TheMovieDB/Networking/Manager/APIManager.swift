//
//  APIManager.swift
//  TheMovieDB
//
//  Created by Kanishk KUMAR on 9/5/2018.
//  Copyright © 2018 mycompany. All rights reserved.
//

import Foundation

enum APIResponse:String {
    case success
    case authenticationError = "Authentication Needed."
    case failed = "Network request failed."
    case badRequest = "Bad request"
    case noData = "No data to decode."
    case unableToDecode = "Sorry, Unable to decode the response."
}

/// Enum used for Success/Failure for API Response
///
/// - success: signifies the sucess response
/// - failure: signifies the failure response
enum Result<String>{
    case success
    case failure(String)
}

/// API Manager, Responsible for Managing API Call's to the server, based on the Environment.
struct APIManager {
    //Change the Environment as per Needs.
    //See: MovieAPIEndPoint.swift
    static let environment : APIEnvironment = .production
    //API Key
    static let MovieAPIKey = "2696829a81b1b5827d515ff121700838"
    let router = Router<MovieApi>()

    /// Allows you to search a movie with a searchTerm.
    /// We call our Router with the page number and handle the completion.
    /// A URLSession returns an error if there is no network or the call to the API could not be made for some reason.
    /// - Parameters:
    ///   - searchTerm: Movie name which user wants to search.
    ///   - page: page number for the results to fetch from API.
    ///   - completion: completion block for results. [apiresponse object / error if any].
    func searchMovies(searchTerm: String, page: Int, completion: @escaping (AnyObject?,_ error: String?)->()){

        router.request(.searchMovies(searchTerm: searchTerm, page: page)) { data, response, error in
            if error != nil {
                let networkError = NSLocalizedString("network_error", comment: "Please check your network connection.")
                completion(nil, networkError)
            }

            if let response = response as? HTTPURLResponse {
                let result = self.handleAPIResponse(response)
                switch result {
                case .success:
                    guard let responseData = data else {
                        completion(nil, APIResponse.noData.rawValue)
                        return
                    }
                    do {
                        let jsonData = try JSONSerialization.jsonObject(with: responseData, options: .mutableContainers)
                        print(jsonData)
                        let apiResponse = try JSONDecoder().decode(MovieApiDataResponse.self, from: responseData)
                        completion(apiResponse as AnyObject, nil)
                    }catch {
                        print(error)
                        completion(nil, APIResponse.unableToDecode.rawValue)
                    }
                case .failure(let apiFailureError):
                    completion(nil, apiFailureError)
                }
            }
        }
    }

    /// API Response Handler
    ///
    /// - Parameter response: HTTPURLResponse
    /// - Returns: reason for failure.
    fileprivate func handleAPIResponse(_ response: HTTPURLResponse) -> Result<String>{

        switch response.statusCode {
        case 200...299: return .success
        case 501: return .failure(APIResponse.badRequest.rawValue)
        case 401: return .failure(APIResponse.authenticationError.rawValue)
        default: return .failure(APIResponse.failed.rawValue)
        }
    }
}
