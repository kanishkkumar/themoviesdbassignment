//
//  MovieDataModel.swift
//  TheMovieDB
//
//  Created by Kanishk KUMAR on 9/5/2018.
//  Copyright © 2018 mycompany. All rights reserved.
//

import Foundation

/// Movies Data Model
struct MovieDataModel {
    let title: String
    let releaseDate: String
    let rating: Double
    let overview: String
    let posterPath: String?
}

// MARK: - MovieDataModel Extension
extension MovieDataModel: Decodable {

    enum MovieCodingKeys: String, CodingKey {
        case title
        case releaseDate = "release_date"
        case rating = "vote_average"
        case overview
        case posterPath = "poster_path"
    }

    init(from decoder: Decoder) throws {
        let movieContainer = try decoder.container(keyedBy: MovieCodingKeys.self)
        do {
            posterPath = try movieContainer.decode(String.self, forKey: .posterPath)
        }
        catch {
            posterPath = ""
        }

        title = try movieContainer.decode(String.self, forKey: .title)
        releaseDate = try movieContainer.decode(String.self, forKey: .releaseDate)
        rating = try movieContainer.decode(Double.self, forKey: .rating)
        overview = try movieContainer.decode(String.self, forKey: .overview)
    }

    /// Poster/Movie Image URL
    var posterUrl: String? {
        get {
            if let path = self.posterPath {
                return "https://image.tmdb.org/t/p/w92\(path)";
            }
            return nil;
        }
    };
}

/// API Response
struct MovieApiDataResponse {
    let page: Int
    let numberOfResults: Int
    let numberOfPages: Int
    let movies: [MovieDataModel]
}

// MARK: - MovieApiDataResponse Extension
extension MovieApiDataResponse: Decodable {

    private enum MovieApiResponseCodingKeys: String, CodingKey {
        case page = "page"
        case numberOfResults = "total_results"
        case numberOfPages = "total_pages"
        case movies = "results"
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: MovieApiResponseCodingKeys.self)
        page = try container.decode(Int.self, forKey: .page)
        numberOfResults = try container.decode(Int.self, forKey: .numberOfResults)
        numberOfPages = try container.decode(Int.self, forKey: .numberOfPages)
        movies = try container.decode([MovieDataModel].self, forKey: .movies)
    }
}
