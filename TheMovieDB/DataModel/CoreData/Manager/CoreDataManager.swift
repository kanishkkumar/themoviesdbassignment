//
//  CoreDataManager.swift
//  TheMovieDB
//
//  Created by Kanishk KUMAR on 13/5/2018.
//  Copyright © 2018 mycompany. All rights reserved.
//

import UIKit
import CoreData

class CoreDataManager {
    static private let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    /// Allows to value in CoreData for the searched textfield
    ///
    /// - Parameter _value: string type, attribute.
    class func saveValueIntoDB(_value: String) {
        if #available(iOS 10.0, *) {
            let context = CoreDataManager.appDelegate.persistentContainer.viewContext
            //Check currently saved suggestions count
            if let fetchedResults = fetchResultsFromDB(), fetchedResults.count > 9, let firstResult = fetchedResults.first {
                context.delete(firstResult)
            }
            let searchedTextEntity = SearchedTextEntity(context: context)
            searchedTextEntity.attribute = _value
            CoreDataManager.appDelegate.saveContext()
        } else {
            let entityDescription =
                NSEntityDescription.entity(forEntityName: "SearchedTextEntity",
                                           in: appDelegate.managedObjectContext)
            //Check currently saved suggestions count
            if let fetchedResults = fetchResultsFromDB(), fetchedResults.count > 9, let firstResult = fetchedResults.first {
                appDelegate.managedObjectContext.delete(firstResult)
            }
            let searchedTextEntity = SearchedTextEntity(entity: (entityDescription)!,
                                                insertInto: appDelegate.managedObjectContext)
            searchedTextEntity.attribute = _value

            do {
                try appDelegate.managedObjectContext.save()
            } catch let error as NSError {
                print(error.localizedFailureReason ?? "Unable to Save Search Term into DB")
            }
        }
    }
    
    /// Allows you to search the value in databse.
    ///
    /// - Returns: return the text of type SearchedTextEntity
    class func fetchResultsFromDB() -> [SearchedTextEntity]? {
        
        if #available(iOS 10.0, *) {
            let context = CoreDataManager.appDelegate.persistentContainer.viewContext
            let fetchRequest: NSFetchRequest<SearchedTextEntity> = SearchedTextEntity.fetchRequest()
            do {
                let results = try context.fetch(fetchRequest)
                if results.count > 0 {
                    return results
                }
                else {
                    return nil
                }
            } catch  {
                return nil
            }
        } else {
            // Fallback on earlier versions
            let entityDescription =
                NSEntityDescription.entity(forEntityName: "SearchedTextEntity",
                                           in: appDelegate.managedObjectContext)
            let request = NSFetchRequest<NSFetchRequestResult>()
            request.entity = entityDescription
            do {
                let results = try appDelegate.managedObjectContext.fetch(request)
                if results.count > 0 {
                    return (results as! [SearchedTextEntity])
                }
                else {
                    return nil
                }
            } catch let error as NSError {
                print(error.localizedFailureReason ?? "Unable to Fetch Data From DB")
            }
        }
        return nil
    }
    
    /// Helper to get values from coredata
    ///
    /// - Returns: returns String value of the searched data from databse.
    class func getSearchTexts() -> [String]? {
        if let fetchedResults = CoreDataManager.fetchResultsFromDB() {
            let stringsArray = fetchedResults.map{$0.attribute ?? ""}
            if stringsArray.count > 0 {
                return stringsArray
            }
            else {
                return nil
            }
        }
        return nil
    }
}

