//
//  SearchedTextEntity+CoreDataProperties.swift
//  
//
//  Created by Kanishk KUMAR on 13/5/2018.
//
//

import Foundation
import CoreData


extension SearchedTextEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<SearchedTextEntity> {
        return NSFetchRequest<SearchedTextEntity>(entityName: "SearchedTextEntity")
    }

    @NSManaged public var attribute: String?

}
