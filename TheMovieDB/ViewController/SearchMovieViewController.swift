//
//  SearchMovieViewController.swift
//  TheMovieDB
//
//  Created by Kanishk KUMAR on 8/5/2018.
//  Copyright © 2018 mycompany. All rights reserved.
//

import UIKit
import MLPAutoCompleteTextField

class SearchMovieViewController: UIViewController {

    // MARK: - Properties
    var searchHistory: [String]?

    // MARK: - IBOutlets
    @IBOutlet weak var searchBtn: UIButton!
    @IBOutlet weak var searchTextField: MLPAutoCompleteTextField!

    // MARK: - ViewController LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitials()
    }

    /// This method is called before the view controller's view is about to be added to a view hierarchy and before any animations are configured for showing the view.
    ///
    /// - Parameter animated: If true, the view is being added to the window using an animation.
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        searchHistory = CoreDataManager.getSearchTexts()
    }

    // MARK: - Instance Methods
    func setupInitials() {
        //Set delegates to self
        searchTextField.autoCompleteDelegate = self
        //Set datasource to self
        searchTextField.autoCompleteDataSource = self
        //Screen Title
        title = NSLocalizedString("themoviesdb", comment: "The MoviesDB")
        //Search fieldPlaceholder
        self.searchTextField.placeholder = NSLocalizedString("search_movie_placeholder_text", comment: "Type a movie name to search")
        //Search Button Title
        let searchBtnTitle = NSLocalizedString("search_btn_title", comment: "Search")
        self.searchBtn.setTitle(searchBtnTitle, for: UIControlState.normal)
        //Navigation Back button
        let backItem = UIBarButtonItem()
        backItem.title = NSLocalizedString("BACK", comment: "Back")
        navigationItem.backBarButtonItem = backItem
    }

    // MARK: - IBActions
    @IBAction func onClickSearch(_ sender: Any) {

        searchTextField.resignFirstResponder()
        //Error Handling
        guard let text = searchTextField.text, !text.isEmpty else {
            DispatchQueue.main.async {
                let noSearchTermErrorMessage = NSLocalizedString("search_term_empty", comment: "Please type some movie to search!")
                let errorTitle = NSLocalizedString("Error", comment: "Error")
                let okBtnTitle = NSLocalizedString("OK", comment: "OK")
                //Show Error Message to user.
                self.presentAlertWithTitle(title: errorTitle, message: noSearchTermErrorMessage, options: okBtnTitle, completion: {_ in })
            }
            return
        }

        //Save the Text searched in CoreData
        CoreDataManager.saveValueIntoDB(_value: self.searchTextField.text!)

        DispatchQueue.main.async {
            //GoToNext Screen
            self.performSegue(withIdentifier: "MoviesListVC", sender: Any?.self)
        }
    }

    // MARK: - Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let moviesListVC = segue.destination as? MoviesListViewController {
            moviesListVC.searchedText = searchTextField.text
        }
    }
}

// MARK: - UITextField Delegate And DataSource
extension SearchMovieViewController: MLPAutoCompleteTextFieldDataSource, MLPAutoCompleteTextFieldDelegate {

    func autoCompleteTextField(_ textField: MLPAutoCompleteTextField!, possibleCompletionsFor string: String!) -> [Any]! {
        return searchHistory != nil ? searchHistory!.reversed() : [String]()
    }

    func autoCompleteTextField(_ textField: MLPAutoCompleteTextField!, didSelectAutoComplete selectedString: String!, withAutoComplete selectedObject: MLPAutoCompletionObject!, forRowAt indexPath: IndexPath!) {
        textField.text = selectedString
        onClickSearch(textField)
    }
}

