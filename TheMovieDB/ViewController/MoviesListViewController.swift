//
//  MoviesListViewController.swift
//  TheMovieDB
//
//  Created by Kanishk KUMAR on 11/5/2018.
//  Copyright © 2018 mycompany. All rights reserved.
//

import Foundation
import UIKit
import IJProgressView
import AlamofireImage

struct Constants {
    static let kCellHeight = 190;
    static let MoviesTableViewCellIdentifier = "MoviesTableViewCell"
}

class MoviesListViewController: UIViewController {

    // MARK: - Properties
    var moviesArray = [MovieDataModel]()
    var searchedText: String?
    var totalPages = 0
    var lastPageIndex = 1
    var apiManager: APIManager!
    var moviesDataAPIResponse :MovieApiDataResponse!
    let errorTitle = NSLocalizedString("Error", comment: "Error")
    let okBtnTitle = NSLocalizedString("OK", comment: "OK")

    // MARK: - IBOutlets
    @IBOutlet weak var iTableView: UITableView!

    // MARK: - ViewController LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitials()
        loadNextPage(lastPageIndex)
    }

    // MARK: - Instance Methods
    func setupInitials() {
        title = self.searchedText
        self.apiManager = APIManager()
        self.iTableView.estimatedRowHeight = CGFloat(Constants.kCellHeight);
        self.iTableView.rowHeight = UITableViewAutomaticDimension;
    }

    /// Allows to Load next page by requesting the data for next page from API
    ///
    /// - Parameter pageNumber: pagenumber for which data needs to be fetched.
    func loadNextPage(_ pageNumber: Int) {

        //Show Loaading
        DispatchQueue.main.async {
            IJProgressView.shared.showProgressView(self.view)
        }
        if let search = self.searchedText {
            self.apiManager.searchMovies(searchTerm: search, page: pageNumber) { apiResponse, error in

                guard apiResponse != nil else {
                    //API Response Failed, Show Error Message to User.
                    DispatchQueue.main.async {
                        IJProgressView.shared.hideProgressView()
                        self.presentAlertWithTitle(title: self.errorTitle, message: error!, options: self.okBtnTitle, completion: {_ in })
                    }
                    return
                }

                if let apiResponse = apiResponse {
                    self.moviesDataAPIResponse = apiResponse as! MovieApiDataResponse;
                    guard self.moviesDataAPIResponse.movies.count > 0 else {
                        //No Results found, Show Error Message to User.
                        DispatchQueue.main.async {
                            IJProgressView.shared.hideProgressView()
                            let errorMessage = NSLocalizedString("no_movies_found", comment: "We couldn't find any related movies, Please try another Keyword.!")
                            self.presentAlertWithTitle(title: self.errorTitle, message: errorMessage, options: self.okBtnTitle, completion: {_ in })
                        }
                        return
                    }

                    self.lastPageIndex = self.moviesDataAPIResponse.page
                    self.totalPages = self.moviesDataAPIResponse.numberOfPages;
                    self.moviesArray.append(contentsOf: self.moviesDataAPIResponse.movies)

                    DispatchQueue.main.async {
                        self.iTableView.reloadData()
                        IJProgressView.shared.hideProgressView()
                    }
                }
            }
        }
    }
}

// MARK: - UITableViewDataSource, UITableViewDelegate
extension MoviesListViewController: UITableViewDataSource, UITableViewDelegate {

    /// Asks the data source to return the number of sections in the table view.
    ///
    /// - Parameter tableView: An object representing the table view requesting this information.
    /// - Returns: The number of sections in tableView. The default value is 1.
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    /// Tells the data source to return the number of rows in a given section of a table view.
    ///
    /// - Parameters:
    ///   - tableView: The table-view object requesting this information.
    ///   - section: An index number identifying a section in tableView.
    /// - Returns: The number of rows in section.
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return moviesArray.count
    }

    /// Asks The data source for a cell to insert in a particular location of the table view.
    ///
    /// - Parameters:
    ///   - tableView: tableView, A table-view object requesting the cell.
    ///   - indexPath: An index path locating a row in tableView.
    /// - Returns: cell
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        //Returns a reusable table-view cell object located by its identifier.
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.MoviesTableViewCellIdentifier, for: indexPath) as? MoviesTableViewCell  else {
            fatalError("Cell is not dequeued.")
        }
        cell.setupCell(_data: moviesArray[indexPath.row])
        
        if indexPath.row == moviesArray.count - 1 &&
            self.lastPageIndex < self.totalPages {
            loadNextPage(self.lastPageIndex + 1)
        }
        return cell
    }
}
