//
//  MoviesTableViewCell.swift
//  TheMovieDB
//
//  Created by Kanishk KUMAR on 12/5/2018.
//  Copyright © 2018 mycompany. All rights reserved.
//

import UIKit

class MoviesTableViewCell: UITableViewCell {

    //MARK: IBOutLets
    @IBOutlet weak var movieTitleLabel: UILabel!
    @IBOutlet weak var movieReleaseDate: UILabel!
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var movieOverview: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func setupCell(_data: MovieDataModel) {
        let movies = _data
        //Title
        movieTitleLabel.text = movies.title
        
        //Release Date, Rating
        let str = "\(movies.releaseDate), Rating:\(movies.rating)"
        movieReleaseDate.text = str
        
        //OverView
        movieOverview.text = movies.overview
        
        //Poster Image
        photoImageView.image = nil
        let imageURL = URL (string: movies.posterUrl!)
        photoImageView.af_setImage(withURL: imageURL!)
    }
}
