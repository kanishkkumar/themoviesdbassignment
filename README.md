### TheMoviesDB ###

===========================================================================

Project Name: TheMoviesDB
Release Date: 14-May-2018
Version: 1.0
Build: 1.0
Devices: Universal (iPhone, iPad)
iOS: iOS 9 and above.

===========================================================================

**DESCRIPTION:**

The TheMoviesDB  allows you to search the Movies, and gives you the results with ratings and overview from IMDB databse.

For more information on TheMoviesDB  data, [see:](https://www.themoviedb.org/)

===========================================================================

**BUILDING A TheMoviesDB:**

To build the Application:
Select "TheMoviesDB" as the active target in Xcode and select "Build" from the Build menu.
 If you experience build errors, that probably is due to not installing pods/dependencies used for that target. 
 In this case, go to the Active directory of the Application in terminal and run "pod install". 
 This will install the dependencies used in the project.

**STEPS:**
```
1. Open Terminal.
2. Enter $ sudo gem install cocoapods command in terminal.
3. Navigate to directory containing Xcode project. Use cd “../directory-location/..” or cd [drag-and-drop project folder]
4: pod install.
```
[FYR]: (https://cocoapods.org/)

===========================================================================

### **Localisation** ###
Allows you to use Localisation within the Application.
Supprted LAnguages: [English,Chinese,French]
To change the Language follow the below steps:
```
1. Edit the Tageted Scheme named "TheMoviesDB"
2. Go To Run -> options -> change the "application language" to the supported Languages, else default language will be used.
```
===========================================================================

### **Running Unit Tests** ###

To run unit tests,
1. From the Scheme pop-up menu in the toolbar, select TheMovieDB (iOS) > <device_simulator>
2. Use either of the following approaches to proceed:
a. Choose Product > Test to run all the test cases implemented, then navigate to View > Navigators > Show Report Navigator to view the test results.
See [Xcode Help > Run and Debug > View and filter logs and reports](http://help.apple.com/xcode/mac/current/#/dev21d56ecd4) for more information.

b. Choose View > Navigators > Show Test Navigator to navigate to the Test navigator. Hover the pointer over any test target or test class to display a run button, then click
the button to run the tests. 
See [Xcode Help > Run UI tests and unit tests](http://help.apple.com/xcode/mac/current/#/dev42b289fbc) for more information.

===========================================================================

**BUILD REQUIREMENTS:**

iOS SDK 9.0 or later

===========================================================================================
**CHANGES FROM PREVIOUS VERSIONS:**

1.0 - Initial First version.

===========================================================================

**Third party Libraries:**

**MBProgressHUD**: MBProgressHUD an iOS drop-in class that displays a translucent HUD with an indicator and/or labels while work is being done in a background thread. 
The HUD is meant as a replacement for the undocumented, private UIKit UIProgressHUD with some additional features.
[For more information on see:](https://github.com/jdg/MBProgressHUD)

**AlamofireImage** : AlamofireImage is an image component library for Alamofire.
[For more information on see:](https://github.com/Alamofire/AlamofireImage)

**MLPAutoCompleteTextField**: MLPAutoCompleteTextField is a subclass of UITextField that behaves like a typical UITextField with one notable exception: it manages a drop down table of autocomplete suggestions that update as the user types. Its behavior may remind you of Google's autocomplete search feature. As of version 1.3 there is also support for showing the autocomplete table as an accessory view of the keyboard.
[For more information on see](https://github.com/EddyBorja/MLPAutoCompleteTextField)

===========================================================================

**Testing Framework**

**Quick**: Quick is a behavior-driven development framework for Swift and Objective-C. Inspired by RSpec, Specta, and Ginkgo.
[For more information on see:](https://github.com/Quick/Quick)
**Nimble**:Use Nimble to express the expected outcomes of Swift or Objective-C expressions. Inspired by Cedar.
[For more information on see:](https://github.com/Quick/Nimble)

===========================================================================

Copyright (C) 2018-kanishkkumar Inc. All rights reserved.
